require 'json'

file = File.read('./pokemons.json')

poke_json = JSON.parse(file)
arr_pokemons = poke_json['results']

poke_names = Array.new

# EXERCÍCIO 1

arr_pokemons.each do |names|
    poke_first_name = names['name'].split('-')[0]
    poke_names.append poke_first_name
end

unique_poke_names = poke_names.uniq
puts "Resultado do exercício 1:"
puts "Existem #{unique_poke_names.length} pokemons distintos!"
puts "\n"

# EXERCÍCIO 2

freq_names = poke_names.inject(Hash.new(0)) { |h,v| h[v] += 1 ; h}
top_pokemons = freq_names.sort_by { |poke, num| num }.reverse

puts "Resultado do exercício 2:"
for i in 0...10 do
    puts "#{i+1}. #{top_pokemons[i][0].capitalize}: #{top_pokemons[i][1]} ocorrências"
end
puts"\n"

# EXERCÍCIO 3

first_letters = Array.new

unique_poke_names.each do |names|
    first_letters.append names[0]
end

freq_letters = first_letters.inject(Hash.new(0)) { |h,v| h[v] += 1 ; h}
top_letters = freq_letters.sort_by { |letter, num| num }.reverse

puts "Resultado do exercício 3:"
for i in 0...10 do
    puts "#{i+1}. #{top_letters[i][0].capitalize}: #{top_letters[i][1]} ocorrências"
end